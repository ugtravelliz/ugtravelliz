import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {

    meta: [
        
      
      { charset: 'utf-8' },
     { name: 'theme-color' , content: '#000000' },
     { name: 'google-site-verification', content: 'g0hvo01v2-NhqfIR6EFmngcgmVNzolJ03GX-By0z3N4' },
     
     { name: 'msapplication-navbutton-color' , content: '#000000' },
     { name: 'apple-mobile-web-app-capable' , content: 'yes' },
     { name: 'apple-mobile-web-app-status-bar-style'  , content: '#000000' },

     { name: 'viewport', content: 'width=device-width, initial-scale=1' },
     { hid: 'description', name: 'description', content: '' },
     { hid: 'description', name: 'keywords', content: '' },

     { property:'og:locale' , content:'af-ZA' },
     { property:'og:type' ,content:'website' },
     { property:'og:title' ,content:'Ekyama Adventures| Small Group Travel and Adventure Company  ' },
     { property: 'og:description' ,content:'Planning A Safari,Simple Trip ? Ekyamas Specialists have been creating custom Safaris and Trips for Come Book Yours' },
     { property:'og:site_name' ,content:'ekyamaadventures' },
     { property:'og:url' ,content: 'https://ekyamaadventures.com/'},
     {name:'twitter:card' ,content: 'summary'},
     {name:'twitter:description', content:'Planning A Safari,Simple Trip ? Ekyamas Specialists have been creating custom Safaris and Trips for Come Book Yours'},
     { name:'twitter:creator' ,content: '@ekyamaadventur1'},
     {name: 'twitter:site', content: '@ekyamaadventures'},
     { name:'twitter:title', content:'Ekyama Adventures| Small Group Travel and Adventure Company'},
     { name:'twitter:image', content:'http://source.unsplash.com/Tvo1NEmfYB0'},
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel:'stylesheet', href:'https://fonts.googleapis.com/css?family=Caveat|Domine|Hind|Indie+Flower|Quicksand|Roboto&display=swap'}
     
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
      '~/assets/styles.css',
      '~/assets/main.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: "~/plugins/vue-masonry-css", ssr: false },
    { src: '~plugins/aoss.js', ssr: false},
    { src: "~/plugins/SocialSharing.js", mode: 'client'}
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
       //'@nuxtjs/onesignal',
    '@nuxtjs/pwa',
  ],
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  
   manifest: {
      short_name: 'ekyamaadventures',
    name: 'ekyamaadventures',
    lang: 'en',
    start_url: '/articles/',
  background_color: '#000000',
  display: 'standalone',
  scope: '/articles/',
  theme_color: '#000000'

  },
   icon: {
    /* icon options */
    iconSrc:'/assets/icon.png',
  },
   workbox: {
    /* workbox options */
    runtimeCaching:[
        {
            urlPattern:'https://fonts.googleapis.com/.*',
            handler:'cacheFirst',
            method:'GET',
            strategyOptions:{ cacheableResponse:{statuses:[0,200]}}
            
        },
        ]
  },


  vuetify: {
    //customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
